import React, {Fragment} from 'react';

import './Alert.css';
import Backdrop from "../Backdrop/Backdrop";

const Alert = props => {
  return (
      <Fragment>
          <Backdrop show={props.show} onClick={props.close}/>
          <div
            className={['Alert', props.alertType].join(' ')}
            style={
            {transform: props.show ? 'TranslateY(0)' : 'TranslateY(100vh)',
              opacity: props.show ? '1': '0'
            }}>
              {props.text}
            {props.dismiss && <button className='alertClose' onClick={props.dismiss}>X</button>}
          </div>
      </Fragment>
  );
};

export default Alert;