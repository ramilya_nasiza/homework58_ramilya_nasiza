import React, {Fragment} from 'react';

import './Modal.css';
import Backdrop from "../Backdrop/Backdrop";

const Modal = props => {
  return (
      <Fragment>
        <Backdrop show={props.show} onClick={props.close}/>
        <div
            className='Modal'
            style={
              {transform: props.show ? 'TranslateY(0)' : 'TranslateY(100vh)',
                opacity: props.show ? '1': '0'
              }}>
          <button className='closedBtn' onClick={props.closed}>X</button>
          <h3 className='title'>{props.title}</h3>
          <p>Some modal text</p>
          {props.children}
        </div>
      </Fragment>

  );
};

export default Modal;