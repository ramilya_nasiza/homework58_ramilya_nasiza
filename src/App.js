import React, { Component } from 'react';

import './App.css';
import PageBuilder from "./container/PageBuilder/PageBuilder";

class App extends Component {
  render() {
    return (
      <div className="App">
        <PageBuilder/>
      </div>
    );
  }
}

export default App;
