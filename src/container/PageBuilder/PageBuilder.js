import React, { Component } from 'react';
import Modal from "../../components/UI/Modal/Modal";


import './PageBuilder.css';
import Button from "../../components/UI/Button/Button";
import Alert from "../../components/UI/Alert/Alert";

class PageBuilder extends Component {

  state={
    isShow: false,
    btns: [
      {type: 'Primary', label: 'Continue', clicked: () => this.getAlert()},
      {type: 'Danger', label: 'Close', clicked: () => this.getClosed()}
    ],
    alertBtns: [
      {type: 'primary', text: 'primary button', alertShow: false},
      {type: 'success', text: 'success button', alertShow: false},
      {type: 'danger', text: 'danger button', alertShow: false},
      {type: 'warning', text: 'warning button ', alertShow: false}
    ]
  };

  getOpen = () => {
    this.setState({isShow: true});
  };

  getClosed = () => {
    this.setState({isShow: false});
  };

  getAlert = () => {
    alert('Let continue!');
  };


  alertClicked = index => {
    let alertBtns = [...this.state.alertBtns];
    let oneBtn = {...alertBtns[index]};
    oneBtn.alertShow = true;
    alertBtns[index] = oneBtn;

    this.setState({alertBtns});
  };

  getHideAlert = (index) => {
    let alertBtns = [...this.state.alertBtns];
    let oneBtn = alertBtns[index];
    oneBtn.alertShow = false;

    this.setState({alertBtns});
  };

  render() {
    return (
        <div className='builder'>
          <button onClick={this.getOpen} className='modalBtn'>Click to show modal</button>
          <Modal
            show={this.state.isShow}
            close={this.getClosed}
            title={'This is a modal title'}
            closed={this.getClosed}
          >
            <Button
            onClick={this.state.btns[0].clicked}
            btnType={this.state.btns[0].type}>
              {this.state.btns[0].label}
            </Button>
            <Button
                onClick={this.state.btns[1].clicked}
                btnType={this.state.btns[1].type}>
              {this.state.btns[1].label}
            </Button>
          </Modal>
          <div>
            {this.state.alertBtns.map((btn, index) => {
              return (
                  <Button
                      key={index}
                      btnType={btn.type}
                      onClick={() => this.alertClicked(index)}>
                    {btn.text}
                  </Button>
              );
            })}
          </div>
          {this.state.alertBtns.map((item, index) => {
            return(
                <Alert
                    show={item.alertShow}
                    close={() => this.getHideAlert(index)}
                    alertType={item.type}
                    text={item.text}
                    key={index}
                    dismiss={() => this.getHideAlert(index)}
                    hideAlert={() => this.getHideAlert(index)}
                />
            );
          })}
        </div>
    );
  }
}

export default PageBuilder;
